
let cursorPos = {
	x: 0,
	y: 0
};

module.exports = {
	log: (...args) => {
		console.log.apply(console, args);
		cursorPos.y = cursorPos.y + 1;
	},
	cursorPos
};
